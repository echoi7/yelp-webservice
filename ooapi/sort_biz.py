import json

with open('biz.json', 'w') as f:
	with open('dataset/business.json') as handle:
		for line in handle:
			business = json.loads(line)
			for cat in business['categories']:
				if cat == 'Restaurants':
						json.dump(business,f)
						f.write('\n')

