Eugene Choi
Maria Beatriz Zanardo

RESTful API

_yelp_database.py first organizes data into three dictionaries: bizs, users, and reviews. Bizs is built from business.json file and contains business_id, neighborhood, city, state, postal_code, stars, review_count, and categories as keys. It then can provide the user information about specific businesses given the business_id (get_biz) or the location of that business (get_location). It can also give the user a dictionary of businesses given the location (neighborhood, city, state, or postal_code) (get_location_biz). Users is built from user.json file and contains user_id, name, review_count, and average_stars. Using this dictionary, the user can retrieve information about a particular user given the user_id (get_user). Reviews is built from review.json file and contains review_id, business_id, user_id, stars, text, cool, funny, and useful. The user can get information about a specific review given the review_id (get_review) or all the reviews for a specific business given the business_id (get_biz_reviews).
